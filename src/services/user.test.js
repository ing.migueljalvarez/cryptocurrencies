import md5 from 'md5'
import User from '../models/user'
import {
  find as userFind,
  findAll as userFindAll,
  insert as userInsert,
  patch as userPatch,
  destroy as userDestroy,
} from './user'

const newUser = {
  firstname: 'example',
  lastname: 'example',
  username: 'example',
  password: '1234abcd',
  preferredCurrency: 'usd',
}
const userWithRepeatUsername = {
  firstname: 'example2',
  lastname: 'example2',
  username: 'example',
  password: '1234abcd',
  preferredCurrency: 'usd',
}
const mockUser = {
  id: expect.anything(Number),
  firstname: 'example',
  lastname: 'example',
  username: 'example',
  password: md5(newUser.password),
  preferredCurrency: 'usd',
  deleted: false,
  createdAt: expect.anything(Date),
  updatedAt: expect.anything(Date),
}
const mockUserWithDeleted = {
  ...mockUser, deleted: true 
}
const mockFindAllUser = {
  docs: [{ ...mockUser }],
  pages: expect.anything(Number),
  total: expect.anything(Number),
}
const mockFindAllUserWithDeleted = {
  docs: [{ ...mockUser, deleted: true }],
  pages: expect.anything(Number),
  total: expect.anything(Number),
}
const mockFindAllEmptyUser = {
  docs: [],
  pages: expect.anything(Number),
  total: expect.anything(Number),
}
let id
describe('All tests that handle in user usage', () => {
  beforeAll(async () => {
    const user = await User.findOne({})
    if (user) {
      await User.destroy({ where: {} })
    }
  })
  afterAll(async () => {
    await User.drop({ cascade: true })
  })
  test('Inserting a valid user should return a json object', async () => {
    const user = JSON.parse(JSON.stringify(await userInsert(newUser)))
    id = user.id
    expect(user).toEqual(mockUser)
  })

  test('Creating a user with a preferred currency other than "eur, usd, ars" should return a validation error', async () => {
    expect(
      userInsert({ ...newUser, preferredCurrency: 'ves' }),
    ).rejects.toThrow(
      new Error(
        'invalid input value for enum "enum_Users_preferredCurrency": "ves"',
      ),
    )
  })

  test('Creating a user with repeated username should throw a validation error.', async () => {
    expect(userInsert(userWithRepeatUsername)).rejects.toThrow(
      new Error('Validation error'),
    )
  })

  test('Searching all users should return an array with the total number of registered users', async () => {
    const users = JSON.parse(JSON.stringify(await userFindAll({})))
    expect(users).toEqual(mockFindAllUser)
  })

  test('Search user by id should return a json object', async () => {
    const user = JSON.parse(JSON.stringify(await userFind(id, {})))
    expect(user).toEqual(mockUser)
  })

  test('Deleting a user without using the hardDeleted parameter should do a soft deleted', async () => {
    const user = JSON.parse(JSON.stringify(await userDestroy(id, {})))
    expect(user).toEqual(mockUserWithDeleted)
  })

  test('Searching for a user who was disabled without the corresponding parameters should return a null', async () => {
    const user = JSON.parse(JSON.stringify(await userFind(id, {})))
    expect(user).toBeNull()
  })
  test('Searching for a user that was disabled withDeleted = true should return the disabled user', async () => {
    const user = JSON.parse(
      JSON.stringify(await userFind(id, { withDeleted: true })),
    )
    expect(user).toEqual(mockUserWithDeleted)
  })
  test('Searching for all users that are disabled should return an object with empty docs and pages at 0 and total at 0', async () => {
    const user = JSON.parse(JSON.stringify(await userFindAll({})))
    expect(user).toEqual(mockFindAllEmptyUser)
  })
  test('Finding all users using withDeleted = true should return a paginated json object', async () => {
    const user = JSON.parse(
      JSON.stringify(await userFindAll({ withDeleted: true })),
    )
    expect(user).toEqual(mockFindAllUserWithDeleted)
  })
})
