import md5 from 'md5'
import User from '../models/user'
import AplicationError from '../utils/aplicationError'

const verifyUser = async (user, password) => {
  if (user.password === md5(password)) {
    return user
  }
  throw new AplicationError('Wrong username/password combination', 401)
}

const auth = async (username, password) => {
  const user = await User.findOne({
    username,
  })

  if ((await user) === null) {
    throw new AplicationError('user not registered / not verified', 401)
  }

  return verifyUser(user, password)
}
const insert = async (data) => {
  if (data.password.length < 8) {
    throw new AplicationError(
      'The Password must be at least 8 characters long',
      400,
    )
  }
  const user = await User.create({
    ...data,
    password: md5(data.password),
  })
  return user
}

const findAll = async ({
  withDeleted = false,
  onlyDeleted = false,
  page = 1,
  ...query
} = {}) => {
  let users
  const options = {
    page,
    paginate: 25,
    where: query,
  }
  if (onlyDeleted) {
    options.where = { ...query, deleted: true }
  }
  if (withDeleted) {
    users = await User.paginate(options)
    return users
  }
  options.where = { ...query, deleted: false }
  users = await User.paginate(options)
  return users
}

const find = async (id, { withDeleted = false, ...query } = {}) => {
  let queryParameters = {}
  if (withDeleted) {
    queryParameters = { ...query, id }
  } else {
    queryParameters = { ...query, id, deleted: false }
  }
  const user = await User.findOne({
    where: queryParameters,
  })
  return user
}

const patch = async (id, { resetPassword = false, restore =false, ...fields }) => {
  let data = {}
  let queryFind = {deleted: false}
  if (resetPassword) {
    data = { ...fields, password: md5(fields.password) }
  } else {
    delete fields.password
    data = { ...fields }
  }
  if (restore) {
    delete fields.password
    queryFind = { deleted: true }
    data = { ...fields, deleted: false}
  }

  const document = await find(id, queryFind)
  const user = await document.update({ ...data })
  return user
}

const destroy = async (id, { hardDeleted = false } = {}) => {
  if (hardDeleted) {
    const user = await User.destroy({ where: { id } })
    return user
  }
  const user = await patch(id, { deleted: true })
  return user
}

export { auth, insert, findAll, find, patch, destroy }
