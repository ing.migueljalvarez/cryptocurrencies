import coingecko from '../coingecko/index'
import Coin from '../models/coins'
import AplicationError from '../utils/aplicationError'

const findCoinsId = async ({ name, symbol }) => {
  const lists = await coingecko.listCoinsId()
  if (name || symbol) {
    const result = lists.filter(
      (list) => list.name === name || list.symbol === symbol,
    )
    return result
  }
  return lists
}
const findCoinsMarket = async ({ currency, page, perPage } = {}) => {
  const coins = await coingecko.findCoinsMarket(currency, page, perPage)
  return coins
}
const createCoinWithId = async (userId, id) => {
  const coinDocument = await coingecko.findCoinsById(id)
  if ((await coinDocument) === null) {
    throw new AplicationError(
      'the id provided is invalid, check the `/coins/lists` endpoint for available ids, eg {id: bitcoin}',
      404,
    )
  }
  const data = {
    ...coinDocument,
    UserId: userId,
  }
  const coins = await Coin.create(data)
  return coins
}
const findCoinsById = async (id, userId, { withDeleted = false, ...query }) => {
  let queryParameters = {}
  if (withDeleted) {
    queryParameters = {
      ...query,
      deleted: true,
      id,
      UserId: userId,
    }
  } else {
    queryParameters = {
      ...query,
      deleted: false,
      id,
      UserId: userId,
    }
  }
  const coin = await Coin.findOne({
    where: queryParameters,
  })
  return coin
}
const findAllCoins = async (
  userId,
  { withDeleted = false, onlyDeleted = false, page = 1, ...query },
) => {
  let coins
  const options = {
    page,
    paginate: 25,
    where: query,
  }
  if (onlyDeleted) {
    options.where = { ...query, UserId: userId, deleted: true }
  }
  if (withDeleted) {
    coins = await Coin.paginate(options)
    return coins
  }
  options.where = { ...query, UserId: userId, deleted: false }
  coins = await Coin.paginate(options)
  return coins
}
export {
  findCoinsMarket,
  createCoinWithId,
  findCoinsId,
  findCoinsById,
  findAllCoins,
}
