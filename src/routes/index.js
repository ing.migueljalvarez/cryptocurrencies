import express from 'express'
import jwtMiddleware from '../utils/jwtMiddleware'
import authController from '../controllers/auth'
import userController from '../controllers/user'
import coinsController from '../controllers/coins'

const router = express.Router()

router.get('/', (req, res) => {
  res.send('Api is OK')
})

// Auth
router.post('/login', authController.auth, authController.login)
// User
router.post('/users', userController.insert)
router.get('/users/:id?', userController.find)
router.patch('/users/:id', userController.patch)
router.delete('/users/:id', userController.destroy)
// // Coins
router.post('/coins', jwtMiddleware.verifyToken, coinsController.insert)
router.get(
  '/coins/lists',
  jwtMiddleware.verifyToken,
  coinsController.findIdOnCoinList,
)
router.get(
  '/coins/market',
  jwtMiddleware.verifyToken,
  coinsController.findOnMarket,
)
router.get('/coins/:id?', jwtMiddleware.verifyToken, coinsController.find)

export default router
