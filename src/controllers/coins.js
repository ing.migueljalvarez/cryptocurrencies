import asyncWrap from '../utils/asyncWrap'
import AplicationError from '../utils/aplicationError'
import {
  findCoinsMarket,
  createCoinWithId,
  findCoinsId,
  findCoinsById,
  findAllCoins,
} from '../services/coins'

const findIdOnCoinList = asyncWrap(async (req, res) => {
  const { name, symbol } = req.query
  const coins = await findCoinsId({
    name: name || undefined,
    symbol: symbol || undefined,
  })
  res.json(coins)
})
const findOnMarket = asyncWrap(async (req, res) => {
  const coins = await findCoinsMarket({
    ...req.query,
    page: req.query.page ? req.query.page : 1,
    perPage: req.query.perPage ? req.query.perPage : 25,
    currency: req.accessToken.currency ? req.accessToken.currency : 'usd',
  })
  res.json(coins)
})

const find = asyncWrap(async (req, res) => {
  const userId = req.accessToken.id
  if (!req.params.id) {
    const coins = await findAllCoins(userId, {
      withDeleted: req.query.withDeleted === 'true',
      onlyDeleted: req.query.onlyDeleted === 'true',
      ...req.query,
    })
    res.json(coins)
  } else {
    const coin = await findCoinsById(req.params.id, userId, {
      withDeleted: req.query.withDeleted === 'true',
      ...req.query,
    })
    res.json(coin)
  }
})
const insert = asyncWrap(async (req, res) => {
  const { id } = req.body
  if (id) {
    const coins = await createCoinWithId(req.accessToken.id, id)
    res.json(coins)
  } else {
    throw new AplicationError(
      'id is not in the body of the petition, check the `/coins/lists` endpoint for available ids, eg {id: bitcoin}',
      400,
    )
  }
})
export default {
  insert,
  findIdOnCoinList,
  findOnMarket,
  find,
}
