import jwt from 'jsonwebtoken'
import dotenv from 'dotenv'
import asyncWrap from '../utils/asyncWrap'
import { auth as userAuth } from '../services/user'

dotenv.config({ silent: true })

const { JWT_EXPIRATION_TIME, JWT_SECRET } = process.env

const auth = asyncWrap(async (req, res, next) => {
  const { username, password } = req.body
  const user = await userAuth(username, password)
  req.user = user
  next()
})
const login = asyncWrap(async (req, res) => {
  const payload = {
    id: req.user.id,
    username: req.user.username,
    currency: req.user.preferredCurrency,
  }

  const token = jwt.sign(payload, JWT_SECRET, {
    expiresIn: JWT_EXPIRATION_TIME,
  })

  if (token) {
    res.status(200).json({
      token,
    })
  }
})
export default { auth, login }
