import asyncWrap from '../utils/asyncWrap'
import {
  find as userFind,
  findAll as userFindAll,
  insert as userInsert,
  patch as userPatch,
  destroy as userDestroy,
} from '../services/user'

const find = asyncWrap(async (req, res) => {
  if (!req.params.id) {
    const users = await userFindAll({
      withDeleted: req.query.withDeleted === 'true' ? true : false,
      onlyDeleted: req.query.onlyDeleted === 'true' ? true : false,
      ...req.query,
    })
    res.json(users)
  } else {
    const user = await userFind(req.params.id, {
      withDeleted: req.query.withDeleted === 'true' ? true : false,
      ...req.query,
    })

    res.json(user)
  }
})

const insert = asyncWrap(async (req, res) => {
  const user = await userInsert(req.body)
  res.json(user)
})

const patch = asyncWrap(async (req, res) => {
  const user = await userPatch(req.params.id, req.body, {
    ...req.query,
    resetPassword: req.query.resetPassword === 'true' ? true : false,
    restore: req.query.restore === 'true' ? true : false,
  })
  res.json(user)
})

const destroy = asyncWrap(async (req, res) => {
  const user = await userDestroy(req.params.id, {
    hardDeleted: req.query.hardDeleted === 'true' ? true : false,
  })
  res.json(user)
})
export default {
  find,
  insert,
  patch,
  destroy,
}
