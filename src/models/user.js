import sequelizePaginate from 'sequelize-paginate'
import { DataTypes, Model } from 'sequelize'
import sequelize from '../db/index'

class User extends Model {}
const currencies = ['usd', 'ars', 'eur']

User.init(
  {
    firstname: { type: DataTypes.STRING, allowNull: false },
    lastname: { type: DataTypes.STRING, allowNull: false },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: {
          args: true,
          msg:
            'Username cannot be an empty string. please enter a valid username',
        },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    preferredCurrency: {
      type: DataTypes.ENUM({ values: currencies }),
      allowNull: false,
      validate: {
        notEmpty: {
          args: true,
          msg:
            'The preferred currency cannot be an empty field, please enter a valid value.',
        },
      },
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    sequelize,
    modelName: 'User',
  },
)

sequelizePaginate.paginate(User)

module.exports = User
