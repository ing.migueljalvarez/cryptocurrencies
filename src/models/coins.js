import sequelizePaginate from 'sequelize-paginate'
import { DataTypes, Model } from 'sequelize'
import sequelize from '../db/index'

class Coin extends Model {}
Coin.init(
  {
    name: { type: DataTypes.STRING, allowNull: false },
    symbol: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    usdPrice: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
    },
    arsPrice: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
    },
    eurPrice: {
      type: DataTypes.DECIMAL(10, 2),
      allowNull: false,
    },
    image: { type: DataTypes.STRING, allowNull: false },
    lastUpdated: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    sequelize,
    modelName: 'Coin',
  },
)

sequelizePaginate.paginate(Coin)

module.exports = Coin
