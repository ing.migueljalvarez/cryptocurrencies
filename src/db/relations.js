import User from '../models/user'
import Coin from '../models/coins'

User.hasOne(Coin, { onlyDeleted: 'CASCADE' })
Coin.belongsTo(User)
