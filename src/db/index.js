import Sequelize from 'sequelize'
import config from '../config/config'
const { NODE_ENV } = process.env
let sequelize
if (NODE_ENV === 'test') {
  sequelize = new Sequelize(
    config.db.test.database,
    config.db.test.username,
    config.db.test.password,
    {
      host: config.db.test.host,
      dialect: config.db.test.dialect,
    },
  )
} else {
  sequelize = new Sequelize(
    config.db.development.database,
    config.db.development.username,
    config.db.development.password,
    {
      host: config.db.development.host,
      dialect: config.db.development.dialect,
    },
  )
}

export default sequelize
