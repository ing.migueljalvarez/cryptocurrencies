import dotenv from 'dotenv'
dotenv.config({ silent: true })

const {
  APP_PORT: port = 3000,
  DB_DIALECT,
  NODE_ENV,
  DB_USERNAME,
  DB_PASSWORD,
  DB_PORT,
  DB_DATABASE,
  DB_HOSTNAME,
  DB_TEST_USERNAME,
  DB_TEST_PASSWORD,
  DB_TEST_PORT,
  DB_TEST_DATABASE,
  DB_TEST_HOSTNAME,
  DB_TEST_DIALECT,
} = process.env

const config = {
  app: {
    port,
  },
  db: {
    test: {
      host: DB_TEST_HOSTNAME,
      dbPort: DB_TEST_PORT,
      database: DB_TEST_DATABASE,
      username: DB_TEST_USERNAME,
      password: DB_TEST_PASSWORD,
      dialect: DB_TEST_DIALECT,
      logging: false,
    },
    development: {
      host: DB_HOSTNAME,
      dbPort: DB_PORT,
      database: DB_DATABASE,
      username: DB_USERNAME,
      password: DB_PASSWORD,
      dialect: DB_DIALECT,
      logging: NODE_ENV === 'development',
    },
  },
  apiCoingecko: {
    url: 'https://api.coingecko.com/api/v3',
  },
}

export default config
