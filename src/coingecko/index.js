import axios from 'axios'
import config from '../config/config'

const findCoinsMarket = async (preferredCurrency, page, perPage) => {
  const coins = await axios({
    url: `${config.apiCoingecko.url}/coins/markets?vs_currency=${preferredCurrency}&order=market_cap_desc&per_page=${perPage}&page=${page}&sparkline=false`,
    method: 'get',
  })
  const coinsArray = []
  for (const coin of coins.data) {
    const { id, name, symbol, image } = coin
    const coinJson = {
      id,
      name,
      symbol,
      image,
      price: coin.current_price,
      lastUpdated: coin.last_updated,
    }
    coinsArray.push(coinJson)
  }
  return coinsArray
}
const findCoinsById = async (id) => {
  const coins = await axios({
    url: `${config.apiCoingecko.url}/coins/${id}`,
    method: 'get',
  })
  const coinJson = {
    name: coins.data.name,
    symbol: coins.data.symbol,
    image: coins.data.image.large,
    arsPrice: coins.data.market_data.current_price.ars,
    usdPrice: coins.data.market_data.current_price.usd,
    eurPrice: coins.data.market_data.current_price.eur,
    lastUpdated: coins.data.last_updated,
  }
  return coinJson
}

const listCoinsId = async () => {
  const lists = await axios({
    url: `${config.apiCoingecko.url}/coins/list`,
    method: 'get',
  })
  return lists.data
}
export default { findCoinsMarket, findCoinsById, listCoinsId }
