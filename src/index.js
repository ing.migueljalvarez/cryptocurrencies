import http from 'http'
import { app } from './app'
import config from './config/config'
import { handleFatalError } from './utils/handleError'
import sequelize from './db/index'
import './db/relations'
const { NODE_ENV } = process.env
const db = config.db[NODE_ENV]['database']
process.on('uncaughtException', handleFatalError)
process.on('unhandledRejection', handleFatalError)
sequelize.authenticate().then(() => {
  sequelize.sync({ force: false }).then(() => {
    http.createServer(app).listen(config.app.port, () => {
      console.log(`\u{1F525} App listening on port ${config.app.port}`)
      console.log(`[Database:] connection open to localhost/${db}`)
    })
  })
})
