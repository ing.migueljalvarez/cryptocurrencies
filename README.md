# Cryptocurrencies

Prueba Técnica Wolox Backend - Cryptocurrencies


Este proyecto a sido configurado para trabajar con `docker` y `docker-compose`, por favor asegurese de tenerlo instalado en una de sus versiones mas recientes.
<br />

## 1. Configuración de variables de entorno.

- Copie el archivo `.env.example` a `.env`
- Rellene las variables correspondientes.

| Variables de entorno  | Description                                                                                                                                                   |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `NODE_ENV`            | use cualquira de los siguienes valores, `development` o `test`                                                                                                |
| `APP_PORT`            | indique el puerto en que cual iniciara su aplicación, por defecto `3000`                                                                                      |
| `JWT_SECRET`          | indique una frases secreta que se utilizara para firmar el token.                                                                                             |
| `JWT_EXPIRATION_TIME` | indica el tiempo de expiracion del token, usando el siguiente formato `1h`,`1m` donde `h` representa horas y `m` representa minutos.                          |
| `DB_USERNAME`         | indica el nombre de usuario de la base de datos                                                                                                               |
| `DB_PASSWORD`         | indica el password de usuario de la base de datos                                                                                                             |
| `DB_HOSTNAME`         | indica el host donde esta la base de datos, para el caso de docker se utilizar el nombre del servicio que contiene la base de datos, ejemplo `db`             |
| `DB_PORT`             | puerto de conexion a la base de datos, configure por defecto `4000`                                                                                           |
| `DB_DATABASE`         | nombre de la base de datos a que se requiere connexion.                                                                                                       |
| `DB_DIALECT`          | use por defecto, `postgres`                                                                                                                                   |
| `DB_TEST_USERNAME`    | indica el nombre de usuario de la base de datos donde se ejecutaran los test                                                                                  |
| `DB_TEST_PASSWORD`    | indica el password de usuario de la base de datos donde se ejecutaran los test                                                                                |
| `DB_TEST_HOSTNAME`    | indica el host donde esta la base de datos de test, para el caso de docker se utilizar el nombre del servicio que contiene la base de datos, ejemplo `dbTest` |
| `DB_TEST_PORT`        | puerto de conexion a la base de datos de test, configure por defecto `5000`                                                                                   |
| `DB_TEST_DATABASE`    | nombre de la base de datos de test a la que se requiere acceso.                                                                                               |
| `DB_TEST_DIALECT`     | use por defecto, `postgres`                                                                                                                                   |

  <br/>

## 2. Inicio de proyecto.

- Ejecute `yarn install` para instalar las dependencias del proyecto.
- Para ejecutar el proyecto, debe posicionarse en la carpeta raiz del mismo y ejecutar el comando `docker-compose up -d`
- Ejecute `docker-compose logs -f api`, para visualizar el estado el api, cuando el api este listo se vera un mensaje de la siguiente manera `🔥 App listening on port 3000` seguido de `[Database:] connection open to localhost/<database>`
  <br />

## 3. Ejecucion de pruebas unitarias.

- Asegurese de tener configurada la base de tados de test con las variables de entornos mostradas anteriormente.
- Cambie el `NODE_ENV` a `test` y ejecute nuevamente `docker-compose up -d` para que se conecte a la base de datos de test.
- Ejecute el siguiente comando para correr los test, `docker-compose exec api yarn run test`

## 4. Documentacion del api

El Api esta documentada en `Swagger`, la documentación  esta expuesta atravez del path `/api/docs`